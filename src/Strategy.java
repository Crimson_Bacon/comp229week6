import java.util.*;

public interface Strategy
{
    public int onEvenRow()
    {
            List<Cell> possibleLocs = getClearRadius(a.loc, a.moves);
            int moveCPUChooses = (new Random()).nextInt(possibleLocs.size());
            //a.setLocation(possibleLocs.get(moveCPUChooses));
            return moveCPUChooses;
    }

    public int onOddRow()
    {
        List<Cell> possibleLocs = getClearRadius(a.loc, a.moves);
            if (a.loc.col - 1 != -1)
            {
                //a.setLocation(possibleLocs.get(a.loc.col - 1));
                return a.loc.col - 1;
            }
    }
}